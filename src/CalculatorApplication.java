import java.util.InputMismatchException;
import java.util.Scanner;

public class CalculatorApplication {

    public static void main(String[] args) {

        System.out.println("Please enter the first number:");
        double firstNumber = readOperand();
        System.out.println("Please enter the second number:");
        double secondNumber = readOperand();
        if (secondNumber == 0) {
            while (secondNumber == 0){
                System.out.println("The second input must not equal to zero");
                System.out.println("Please enter the second number:");
                secondNumber = readOperand();
            }

        }
        char operator = readOperator();
        applyOperandOnOperators(firstNumber, secondNumber, operator);

    }

    private static void applyOperandOnOperators(double firstNumber, double secondNumber, char operator) {
        switch (operator) {
            case '+':
                System.out.println(firstNumber + " + " + secondNumber + " is equal to " + (firstNumber + secondNumber));
                break;
            case '-':
                System.out.println(firstNumber + " - " + secondNumber + " is equal to " + (firstNumber - secondNumber));
                break;
            case '*':
                System.out.println(firstNumber + " * " + secondNumber + " is equal to " + (firstNumber * secondNumber));
                break;
            case '/':
                System.out.println(firstNumber + " / " + secondNumber + " is equal to " + (firstNumber / secondNumber));
                break;
            default:
                System.out.println("Undefined!");
                break;
        }
    }

    private static double readOperand() {

        Scanner scanner = new Scanner(System.in);
        double operand = 0;
        try {
            operand = scanner.nextDouble();
        } catch (InputMismatchException ex) {
            System.out.println("The input must be a number, Please enter the number: ");
            readOperand();
        }
        return operand;
    }

    private static char readOperator() {

        System.out.println("Please enter the operator(+, -, *, /):");
        Scanner scanner = new Scanner(System.in);
        String operator = scanner.nextLine();
        if (operator.equalsIgnoreCase("+") || operator.equalsIgnoreCase("-") ||
                operator.equalsIgnoreCase("*") || operator.equalsIgnoreCase("/")) {
            char nextOperator = operator.charAt(0);
            return nextOperator;
        } else {
            readOperator();
        }
        char nextOperator = operator.charAt(0);
        return nextOperator;
    }
}
